import { url_weather_base, api_key } from '../constants/api_url';

const getUrlWeatherByCity = city => {
    return `${url_weather_base}?q=${city}&appid=${api_key}&units=metric`;   
}

export default getUrlWeatherByCity;