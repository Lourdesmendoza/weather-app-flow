import React from 'react';
import PropTypes from 'prop-types';
import './styles.sass';

const WeatherExtraInfo = ({ humidity, wind }) => (
    <div className="weatherExtraInfo__content">
        <span className="weatherExtraInfo__text">{ `Humedad: ${humidity} %` }</span>
        <span className="weatherExtraInfo__text">{ `Vientos: ${wind}` }</span>
    </div>
);

WeatherExtraInfo.propTypes = {
    humidity: PropTypes.number.isRequired,
    wind: PropTypes.string.isRequired,
}
export default WeatherExtraInfo