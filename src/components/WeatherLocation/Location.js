import React from 'react';
import PropTypes from 'prop-types';
import './styles.sass';

const Location = ({ city }) => (
    <div className="location__content">
        <h1 className="location__city">{city}</h1>
    </div>
);

Location.propTypes = {
    city: PropTypes.string.isRequired,
};

export default Location;