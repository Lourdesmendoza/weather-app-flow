import React from 'react';
import PropTypes from 'prop-types';
import WeatherData from '../../WeatherLocation/WeatherData';

const ForecastItem = ({ weekDay, hour, data }) => (
    <div className="forecastItem__content">
        <h2 className="forecastItem__subtitle">{weekDay} - {hour}hs</h2>
        <WeatherData data={data} />
    </div>
);

ForecastItem.propTypes = {
    weekDay: PropTypes.string.isRequired,
    hour: PropTypes.number.isRequired,
    data: PropTypes.shape({
        temp: PropTypes.number.isRequired,
        weatherState: PropTypes.string.isRequired,
        humidity: PropTypes.number.isRequired,
        wind: PropTypes.string.isRequired,
    }),
}

export default ForecastItem;