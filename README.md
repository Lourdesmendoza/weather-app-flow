## Frontend Test 
Preferentemente desarrollar en React.js, como alternativa Vue.js o Angular. 
Desarrollar una app full client-side que permita visualizar el pronóstico climático actual y de los próximos 5 días en la ubicación actual y permita visualizar el pronóstico de otras 5 ciudades seleccionables. 
Se debe idear y diseñar la UI que se considere mas acorde (No se valoran habilidades de diseño, sino de uso de componentes). 
Los datos deben ser consumidos de la API desarrollada (en caso de que la prueba lo requiera) o la API externa (Si solo se evalúa Front).  [Open Weather Map](https://openweathermap.org/api).

## Fundamentos
Weather app se dividio en dos componentes principales `WeatherLocation` donde se renderizan subcomponentes como `WeatherData` donde traen la data que me devuelve la API de Weather Open Maps del pronóstico, y sus ciudades, Y su segundo componente principal `ForcastExtended` donde se renderiza el pronóstico extendido, en éste componente también se desprenden sus subcomponentes donde tiene la data más especifica de cada ciudad de forma extendida.

## Comandos
Para comenzar a correr el proyecto: se debe tirar por terminal

### `npm start`

Para correr test unitario de React se debe tirar por terminal:

### `npm test`

